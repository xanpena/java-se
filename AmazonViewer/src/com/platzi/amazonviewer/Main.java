package com.platzi.amazonviewer;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import com.platzi.amazonviewer.model.Chapter;
import com.platzi.amazonviewer.model.Movie;
import com.platzi.amazonviewer.model.Serie;
import com.platzi.makereport.Report;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Movie movie = new Movie("The Road Trip", "Comedy", (short) 2006);
		// movie.title = "The Road Trip (Radio Encubierta)";
		// movie.showData();

		/*Movie movie = new Movie("Coco", "Animation", "", 120, (short) 2017);
		Movie movie2 = new Movie("Coco", "Animation", "", 120, (short) 2017);
		//System.out.println(movie.toString());
		// if(movie == movie2) { }// No devuelve verdadero por que compara las direcciones de memoria no los objetos en si, hay que usar equal()
		
		if(movie.equals(movie2)) {
			System.out.println(true);
		}else {
			System.out.println(false);
		}
		System.out.println(movie);*/
		
		
		showMenu();
		
	}
	
	public static void showMenu() {
		
		int exit = 0; 
		
		do {
			System.out.println("BIENVENIDO AMAZON VIEWER");
			System.out.println("");
			System.out.println("Selecciona la opci�n que prefieras");
			System.out.println("1. Movies");
			System.out.println("2. Series");
			System.out.println("3. Books");
			System.out.println("4. Magazines");
			System.out.println("5. Make Report");
			System.out.println("6. Make Report Today");
			System.out.println("0. Log out");
			
			// Leer la respuesta del usuario
			
			Scanner sc = new Scanner(System.in);
			int response = Integer.valueOf(sc.nextLine());
			
			
			switch(response){
				case 0:
					exit = 0;
					break;
				case 1:
					showMovies();
					break;
				case 2:
					showSeries();
					break;
				case 3:
					showBooks();
					break;
				case 4:
					showMagazines();
					break;
				case 5:
					makeReport();
					break;
				case 6:
					makeReport(new Date());
					break;
				default:
					System.out.println("Error");
					break;
			}
		}while(exit!=0);
		
		
	}
	
	static ArrayList<Movie> movies;
	public static void showMovies() {
		int exit = 1; 
		
		 movies = Movie.makeMoviesList();
		
		do {
			System.out.println();
			System.out.println(":: MOVIES ::");
			System.out.println();
			
			for(int i=0; i<movies.size(); i++) {
				System.out.println(i + 1 + ". " + movies.get(i).getTitle() + " Visto: " + movies.get(i).isViewed());
			}
			
			System.out.println("0. Regresar al Men�");
			System.out.println();
			
			// Leer respuesta del usuario
			Scanner sc = new Scanner(System.in);
			int response = Integer.valueOf(sc.nextLine());
			
			if(response == 0) {
				exit = 0;
				showMenu();
			}
			
			if(response > 0) {
				Movie movieSelected = movies.get(response-1);
				movieSelected.setViewed(true);
				Date dateI = movieSelected.startToSee(new Date());
				
				for(int i=0; i<1000; i++) {
					System.out.println("........................");
				}
				
				// Fin de pel�cula
				movieSelected.stopToSee(dateI, new Date());
				System.out.println();
				System.out.println("Viste: " + movieSelected);
				System.out.println("Por: " + movieSelected.getTimeViewed() + " milisegundos");
			}
			
		}while(exit!=0);
	}
	
	public static void showSeries() {
		int exit = 1; 
		
		ArrayList<Serie> series = Serie.makeSeriesList();
		do {
			System.out.println();
			System.out.println(":: SERIES ::");
			
			for(int i=0; i<series.size(); i++) {
				System.out.println(i + 1 + ". " + series.get(i).getTitle() + " Visto: " + series.get(i).isViewed());
			}
			
			System.out.println("0. Regresar al Men�");
			System.out.println();
			
			// Leer respuesta del usuario
			Scanner sc = new Scanner(System.in);
			int response = Integer.valueOf(sc.nextLine());
			
			if(response == 0) {
				showMenu();
			}
			
			showChapters(series.get(response-1).getChapters());
			
		}while(exit!=0);
	}
	
	public static void showChapters(ArrayList<Chapter> chaptersOfSerieSelected) {
		int exit = 0; 
		do {
			System.out.println();
			System.out.println(":: CHAPTERS ::");
			System.out.println();
			
			for(int i=0; i<chaptersOfSerieSelected.size(); i++) {
				System.out.println(i+1 + ". " + chaptersOfSerieSelected.get(i).getTitle() + " Visto: " + chaptersOfSerieSelected.get(i).isViewed());
			}
			
			System.out.println("0. Regresar al Men�");
			System.out.println();
			
			// Leer respuesta del usuario
			Scanner sc = new Scanner(System.in);
			int response = Integer.valueOf(sc.nextLine());
			
			if(response == 0) {
				showMenu();
			}
			
			Chapter chapterSelected = chaptersOfSerieSelected.get(response-1);
			chapterSelected.setViewed(true);
			Date dateI = chapterSelected.startToSee(new Date());
			
			for(int i=0; i<1000; i++) {
				System.out.println("........................");
			}
			
			// Fin de pel�cula
			chapterSelected.stopToSee(dateI, new Date());
			System.out.println();
			System.out.println("Viste: " + chapterSelected);
			System.out.println("Por: " + chapterSelected.getTimeViewed() + " milisegundos");
			

			
		}while(exit!=0);
	}
	
	public static void showBooks() {
		int exit = 0; 
		do {
			System.out.println();
			System.out.println(":: BOOKS ::");
		}while(exit!=0);
	}
	
	public static void showMagazines() {
		int exit = 0; 
		do {
			System.out.println();
			System.out.println(":: MAGAZINES ::");
		}while(exit!=0);
	}
	
	public static void makeReport() {
		Report report = new Report();
		
		report.setNameFile("reporte");
		report.setTitle(":: VISTOS ::");
		report.setExtension("txt");
		String contentReport = "";
		
		for(Movie movie : movies) {
			if(movie.getIsViewed()) {
				contentReport += movie.toString() + "\n";
			}
		}
		
		report.setContent(contentReport);
		report.makeReport();
		
		
	}
	
	public static void makeReport(Date date) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = df.format(date);
		Report report = new Report();
		
		report.setNameFile("reporte" + dateString);
		report.setExtension("txt");
		String contentReport = "";
		
		for(Movie movie : movies) {
			if(movie.getIsViewed()) {
				contentReport += movie.toString() + "\n";
			}
		}
		
		report.setContent(contentReport);
		report.makeReport();
	}

}
