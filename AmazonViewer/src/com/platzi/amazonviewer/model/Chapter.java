package com.platzi.amazonviewer.model;

import java.util.ArrayList;

public class Chapter extends Movie {

	private int id;
	private int timeViewed;
	private int season;

	

	public Chapter(String title, String genre, String creator, int duration, short year, int season) {
		super(title, genre, creator, duration, year);
		this.setSeason(season);
	}
	
	public int getId() {
		return this.id;
	}

	public int getSeason() {
		return season;
	}

	public void setSeason(int season) {
		this.season = season;
	}
	
	@Override
	public String toString() {
		return "\n :: CHAPTER :: "
				+ "\n Title: " + getTitle()
				+ "\n Year: " + getYear()
				+ "\n Creator: " + getCreator()
				+ "\n Duration: " + getDuration();
	}
	
	public static ArrayList<Chapter> makeChaptersList(){
		
		ArrayList<Chapter> chapters = new ArrayList();
		
		for(int i= 1; i <= 5; i++) {
			chapters.add(new Chapter("Chapter "+ i, " G�nero "+ i, "Creador " + i, 45, (short)(2017+i), i));
		}
		
		return chapters;
	}

	
	
	

	
	

	
}
