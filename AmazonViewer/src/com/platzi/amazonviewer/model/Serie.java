package com.platzi.amazonviewer.model;

import java.util.ArrayList;

public class Serie extends Film {

	private int id;
	private int seasons; 
	private ArrayList<Chapter> chapters;

	public Serie(String title, String genre, String creator, int duration, int seasons, ArrayList<Chapter> arrayList) {
		super(title, genre, creator, duration);
		this.seasons = seasons;
	}


	public int getId() {
		return id;
	}
	
	public int getSeasons() {
		return seasons;
	}
	
	public void setSeason(int seasons) {
		this.seasons = seasons;
	}
	
	public ArrayList<Chapter> getChapters() {
		return chapters;
	}
	
	public void setChapters(ArrayList<Chapter> chapters) {
		this.chapters = chapters;
	}
	
	@Override
	public String toString() {
		return "\n :: SERIE :: "
				+ "\n Title: " + getTitle()
				+ "\n Genre: " + getGenre()
				+ "\n Year: " + getYear()
				+ "\n Creator: " + getCreator()
				+ "\n Duration: " + getDuration();
	}
	
	public static ArrayList<Serie> makeSeriesList(){
		
		ArrayList<Serie> series = new ArrayList();
		
		for(int i= 1; i <= 5; i++) {
			series.add(new Serie("Serie "+ i, " G�nero "+ i, "Creador " + i, 120, 5, Chapter.makeChaptersList()));
		}
		
		return series;
	}
	
	
	
	
	
}
