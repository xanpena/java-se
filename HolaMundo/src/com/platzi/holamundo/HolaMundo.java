package com.platzi.holamundo;

public class HolaMundo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Hola Mundo :)");
		
		// Enteros
		byte edad = 127;
		short year = -32768;
		int id_user = 1001;
		long id_twitter = 115656614;
		
		// Punto flotante
		float diametro = 34.25F;
		double precio = 1234.4565478521354;
		
		// char
		char genero = 'H';
		
		// l�gico
		boolean isVisible = true; 
		boolean funciona = false;
		
		int variable = 2;
		int Variable = 1;
		int _variable = 3;
		int $variable = 4;
		
		System.out.println($variable);
		
		// Constantes
		int VALOR = 0;
		int VALOR_MAXIMO = 3;
		
		// Cast
		byte b = 6;
		short s = b;
		b = (byte) s;
		
		int i = 1;
		double  d = 2.5;
		i = (int) d; 
		
		System.out.println(i);
		
		int codigo = 97;
		char codigoASCII = (char) codigo;
		
		System.out.println(codigoASCII);
		
		// Arrays
		// Declaraci�n de arrays
		
		int[] arrayInt = new int[3];
		double arrayDouble[];
		
		int[][] array2D = new int[2][3]; // Caben 6 valores
		int[][][] array3D = new int[3][3][2]; // Caben 18 valores
		int[][][][] array4D = new int[1][2][3][4]; // Caben 20 valores
		
		char[][] daysAndMonths = {{'M', 'T', 'W', 'T', 'F', 'S', 'D'}, {'J', 'F', 'M'}};
		
		// �ndices
		
		char [] names = new char[3];
		names[0] = 'h';
		names[1] = 'o';
		names[2] = 'l';
		
		System.out.println(names[1]);
		
		// Operadores Aritm�ticos
		int operando1 = 1;
		int operando2 = 2;
		
		operando1 = operando2 + operando1;
		
		System.out.println("El valor de operando1 es: " + operando1);
		
		double x = 2.56;
		int y = 9;
		float w = (float) x + y;
		
		System.out.println(w);
		System.out.println(w*2);
		
		
		
	}

}
