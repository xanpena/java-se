CREATE DATABASE tasklist;

USE tasklist;

CREATE TABLE `categories` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(120) NOT NULL,
	`created_at` DATE NULL DEFAULT NULL,
	`updated_at` DATE NULL DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE
);

CREATE TABLE `tasks` (
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`category_id` INT UNSIGNED,
	`name` VARCHAR(120) NOT NULL,
	`created_at` DATE NULL DEFAULT NULL,
	`updated_at` DATE NULL DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE
);

ALTER TABLE `tasks` ADD CONSTRAINT `FK_tasks_categories` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

INSERT INTO `categories` (`name`) VALUES ('Work');
INSERT INTO `tasks` (`category_id`, `name`) VALUES ('1', 'Daily');