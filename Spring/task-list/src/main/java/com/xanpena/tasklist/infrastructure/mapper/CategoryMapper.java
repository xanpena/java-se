package com.xanpena.tasklist.infrastructure.mapper;

import com.xanpena.tasklist.infrastructure.entity.Category;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface CategoryMapper {
    @Mappings({
            @Mapping(source="name", target="name")
    })
    Category toCategory(Category category);
}