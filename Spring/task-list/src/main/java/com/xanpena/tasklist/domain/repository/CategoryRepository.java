package com.xanpena.tasklist.domain.repository;

import com.xanpena.tasklist.infrastructure.entity.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository {
    List<Category> getAll();
    Optional<List<Category>> getById(int id);
    Optional<List<Category>> getByName(String category);
}
