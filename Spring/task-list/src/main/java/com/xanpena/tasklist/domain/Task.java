package com.xanpena.tasklist.domain;

public class Task {

    private int id;
    private String task;

    /*
     * Getters
     */
    public String getTask()
    {
        return task;
    }

    public int getId() {
        return id;
    }

    /*
     * Setters
     */
    public void setId(int id) {
        this.id = id;
    }

    public void setTask(String task)
    {
        this.task = task;
    }
}
