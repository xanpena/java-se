package com.xanpena.tasklist.infrastructure.entity;

import jakarta.persistence.*;

import java.time.LocalDate;

@Entity
@Table(name="tasks")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name="category_id")
    private Integer categoryId;
    private String name;
    @Column(name="created_at")
    private LocalDate createdAt;
    @Column(name="updated_at")
    private LocalDate updatedAt;

    @OneToOne
    @JoinColumn(name = "category_id", insertable = false, updatable = false)
    private Category category;

    /*
     * Getters
     */
    public LocalDate getCreatedAt()
    {
        return createdAt;
    }
    public Integer getCategoryId()
    {
        return categoryId;
    }

    public String getName()
    {
        return name;
    }

    public LocalDate getUpdatedAt()
    {
        return updatedAt;
    }

    /*
     * Setters
     */
    public void setCategoryId(Integer categoryId)
    {
        this.categoryId = categoryId;
    }

    public void setName(String name)
    {
        this.name = name;
    }



}
