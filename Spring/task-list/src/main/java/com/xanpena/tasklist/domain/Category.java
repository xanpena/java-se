package com.xanpena.tasklist.domain;

public class Category {

    private int id;
    private String category;

    /*
     * Getters
     */
    public String getCategory()
    {
        return category;
    }

    public int getId() {
        return id;
    }

    /*
     * Setters
     */
    public void setId(int id) {
        this.id = id;
    }

    public void setCategory(String category)
    {
        this.category = category;
    }
}
