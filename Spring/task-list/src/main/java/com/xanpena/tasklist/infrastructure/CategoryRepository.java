package com.xanpena.tasklist.infrastructure;

import com.xanpena.tasklist.infrastructure.crud.CategoryCrudRepository;
import com.xanpena.tasklist.infrastructure.entity.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
// @Component
public class CategoryRepository {
    @Autowired
    private CategoryCrudRepository categoryCrudRepository;

    public List<Category> getAll() {
        return (List<Category>) categoryCrudRepository.findAll();
    }

    public List<Category> getByCategory(int categoryId) {
        return categoryCrudRepository.findById(categoryId);
    }

    public Category save(Category category) {
        return categoryCrudRepository.save(category);
    }

    public void delete(int categoryId) {
        categoryCrudRepository.deleteById(categoryId);
    }
}
