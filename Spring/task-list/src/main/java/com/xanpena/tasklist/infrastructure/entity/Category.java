package com.xanpena.tasklist.infrastructure.entity;

import jakarta.persistence.*;

import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "categories")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;
    @Column(name="created_at")
    private LocalDate createdAt;
    @Column(name="updated_at")
    private LocalDate updatedAt;

    @OneToMany(mappedBy = "category")
    private List<Task> tasks;

    /*
     * Getters
     */
    public LocalDate getCreatedAt()
    {
        return createdAt;
    }
    public String getName()
    {
        return name;
    }

    public LocalDate getUpdatedAt()
    {
        return updatedAt;
    }

    /*
     * Setters
     */
    public void setName(String name)
    {
        this.name = name;
    }
}
