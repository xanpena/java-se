package com.xanpena.tasklist.infrastructure.crud;

import com.xanpena.tasklist.infrastructure.entity.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CategoryCrudRepository extends CrudRepository<Category, Integer> {

    // @Query(value = "SELECT * FROM categories WHERE category_id = ?", nativeQuery = true)
    List<Category> findById(int id);
    //Optional<List<Category>>
}
